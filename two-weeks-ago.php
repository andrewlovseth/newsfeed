<?php 

/*
 
    Template Name: Two Weeks Ago

*/

date_default_timezone_set('America/Los_Angeles');

get_header(); ?>

    <section class="this-week feed grid">
        <div class="page-header">
            <h1>Two Weeks Ago</h1>
        </div>

        <?php
            $offsets = ['-7 days', '-8 days', '-9 days', '-10 days', '-11 days', '-12 days', '-13 days'];
            foreach($offsets as $offset): 
            $date = date('F j, Y', strtotime($offset));
        ?>

            <div class="day">
                <div class="section-header">
                    <h2><?php echo $date; ?></h2>
                </div>


                <?php
                $year = date('Y', strtotime($offset));
                $mon = date('m', strtotime($offset));
                $mday = date('d', strtotime($offset));

                $args = array(
                    'post_type' => 'post',
                    'posts_per_page' => -1,
                    'date_query' => array(
                        array(
                            'year'  => $year,
                            'month' => $mon,
                            'day'   => $mday,
                        ),
                    ),
                );
                $query = new WP_Query( $args );
                if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

                <?php get_template_part('template-parts/global/article'); ?>

            <?php endwhile; endif; wp_reset_postdata(); ?>

            </div>


        <?php endforeach; ?>

    </section>

<?php get_footer(); ?>