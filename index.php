<?php get_header(); ?>

<section class="feed grid">
    <div class="section-header">
        <h2>Today</h2>
    </div>

<?php
    $today = getdate();
    $y_year = date('Y', strtotime("-1 days"));
    $y_mon = date('m', strtotime("-1 days"));
    $y_mday = date('d', strtotime("-1 days"));

    $args = array(
		'post_type' => 'post',
		'posts_per_page' => -1,
        'date_query' => array(
/*            array(
                'year'  => $today['year'],
                'month' => $today['mon'],
                'day'   => $today['mday'],
            ),
*/
            array(
                'year'  => $y_year,
                'month' => $y_mon,
                'day'   => $y_mday,
            ),
        ),
    );
	$query = new WP_Query( $args );
	if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

        <?php
            $source = get_field('source');
        ?>

	<article>
        <div class="date">
            <span><?php the_time('g:ia'); ?></span>
        </div>
        <div class="hed">
            <h3><a href="<?php the_field('link'); ?>" target="window"><?php the_title(); ?></a></h3>
        </div>

        <?php if($source): ?>
            <div class="source">
                <span><?php echo normalize_whitespace($source); ?></span>
            </div>
        <?php endif; ?>

	</article>

<?php endwhile; endif; wp_reset_postdata(); ?>


</section>





<?php get_footer(); ?>