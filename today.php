<?php 

/*
 
    Template Name: Today

*/

date_default_timezone_set('America/Los_Angeles');

$today = getdate();
$weekday = $today['weekday'];
$month = $today['month'];
$day = $today['mday'];
$year = $today['year'];

get_header(); ?>

    <section class="feed grid">
        <div class="page-header">
            <h1>
                Today
                <span class="date"><?php echo $weekday . ', ' . $month . ' ' . $day . ', ' . $year; ?></span>
            </h1>
        </div>

        <?php
            $args = array(
                'post_type' => 'post',
                'posts_per_page' => -1,
                'date_query' => array(
                    array(
                        'year'  => $today['year'],
                        'month' => $today['mon'],
                        'day'   => $today['mday'],
                    ),
                ),
            );
            $query = new WP_Query( $args );
            if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

            <?php get_template_part('template-parts/global/article'); ?>

        <?php endwhile; endif; wp_reset_postdata(); ?>

    </section>

<?php get_footer(); ?>