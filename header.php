<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>

<div id="page" class="site">
	
	<header class="site-header">
		<a href="<?php echo site_url('/'); ?>">Today</a>
		<a href="<?php echo site_url('/yesterday/'); ?>">Yesterday</a>
		<a href="<?php echo site_url('/this-week/'); ?>">This Week</a>
		<a href="<?php echo site_url('/two-weeks-ago/'); ?>">Two Weeks Ago</a>
	</header>

	<main class="site-content">