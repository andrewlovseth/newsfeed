<?php 

/*
 
    Template Name: Yesterday

*/

date_default_timezone_set('America/Los_Angeles');

$weekday = date('l', strtotime("-1 days"));
$month = date('F', strtotime("-1 days"));
$day = date('d', strtotime("-1 days"));
$year = date('Y', strtotime("-1 days"));

get_header(); ?>

    <section class="feed grid">
        <div class="page-header">
            <h1>
                Yesterday
                <span class="date"><?php echo $weekday . ', ' . $month . ' ' . $day . ', ' . $year; ?></span>
            </h1>
        </div>

        <?php
            $y_year = date('Y', strtotime("-1 days"));
            $y_mon = date('m', strtotime("-1 days"));
            $y_mday = date('d', strtotime("-1 days"));

            $args = array(
                'post_type' => 'post',
                'posts_per_page' => -1,
                'date_query' => array(
                    array(
                        'year'  => $y_year,
                        'month' => $y_mon,
                        'day'   => $y_mday,
                    ),
                ),
            );
            $query = new WP_Query( $args );
            if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

            <?php get_template_part('template-parts/global/article'); ?>

        <?php endwhile; endif; wp_reset_postdata(); ?>

    </section>

<?php get_footer(); ?>