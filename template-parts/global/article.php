
<?php

    $source = get_field('source');
    $link = get_field('link');
    $site = parse_url($link);
    $url = $site['host'];


    $prefix = 'www.';
    if (substr($url, 0, strlen($prefix)) == $prefix) {
        $url = substr($url, strlen($prefix));
    } 

?>

<article>
    <div class="date">
        <span><?php the_time('g:ia'); ?></span>
    </div>
    <div class="hed">
        <h3><a href="<?php echo $link; ?>" target="window"><?php the_title(); ?></a></h3>
    </div>

    <?php if($site): ?>
        <div class="source">
            <span class="site"><?php echo $url; ?></span>
        </div>
    <?php endif; ?>
</article>